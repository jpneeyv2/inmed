
$(document).ready(function(){
    redir();
    mobMenu();
})

$(window).scroll(function(){
    header();
})

function header(){
    if($(document).scrollTop() > 100){
        $(".logo").css({
            "width" : "50%",
            "transition" : "0.5s"
        });
        $(".web-a").css({
            "font-size" : "8px",
            "transition" : "0.5s"
        })
    }
    else {
        $(".logo").css({
            "width" : "65%",
            "transition" : "0.5s"
        });
        $(".web-a").css({
            "font-size" : "12px",
            "transition" : "0.5s"
        })
    }
}

function redir(e){
    if(e == 'new_arrival'){
        window.location="./pages/new-arrivals.html";
    }
    if(e == 'careers'){
        window.location="./pages/careers.html";
    }
    if(e == 'news_letter'){
        window.location="./pages/contact-us.html";
    }
    if(e == 'reseller'){
        window.location="./pages/reseller.html";
    }
    if(e == 'promos'){
        window.location="./pages/promos.html";
    }
}

function mobMenu(e) {
    if(e == 1){
        console.log('ee')
        $(".mob-menu").css({
            'top'   :   '0'
        })
    }
    if(e == 2){
        console.log('ee')
        $(".mob-menu").css({
            'top'   :   '-100vh'
        })
    }
}

function imgModal(e){
    switch(e){
        case 1:
            var srcs = $('.img_1').attr('src')
            break;
        case 2:
            var srcs = $('.img_2').attr('src')
            break;
        case 3:
            var srcs = $('.img_3').attr('src')
            break;
        default:
            location.reload();
            break;
        }
    $(".pop-up-img").attr("src", srcs);
    $('.pop-up-img').fadeIn(100);
    $('.pop-holder').fadeIn(100);
    $('.pop-holder').on('click', function(){
        $('.pop-up-img').fadeOut(100);
        $('.pop-holder').fadeOut(100);
    })
}

$(document).ready(function () {
    var $form = $('.inmedNews')
    if ($form.length > 0) {
        $('.inmedNews input[type="submit"]').bind('click', function (event) {
        if (event) event.preventDefault()
        register($form)
        })
    }
    })

    function register($form) {
    $('#embedded-subscribe').val('Sending...');
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        cache: false,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        error: function (err) { alert('Could not connect to the registration server. Please try again later.') },
        success: function (data) {
        $('#embedded-subscribe').val('subscribe')
        if (data.result === 'success') {
            $('#mce-EMAIL').css('borderColor', '#ffffff')
            $('#subscribe-result').css('color', 'rgb(250, 250, 250)')
            $('#subscribe-result').html('<p>Thank you for subscribing !</p>')
            $('#mce-EMAIL').val('')
        } else {
            $('#mce-EMAIL').css('borderColor', '#ff8282')
            $('#subscribe-result').css('color', '#ff8282')
            $('#subscribe-result').html('<p>' + data.msg.substring(4) + '</p>')
        }
        }
    })
};
